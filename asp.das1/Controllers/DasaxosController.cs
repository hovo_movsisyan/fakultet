﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using asp.das1.Models;

namespace asp.das1.Controllers
{
    public class DasaxosController : Controller
    {
        FakultetEntities context = new FakultetEntities();
        // GET: Dasaxos
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult Cucak()
        {
            ViewBag.dasaxosner = context.Dasaxos.ToList();
            return View();
        }
        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.ambionner = context.Ambion.ToList();

            return View();
        }
        [HttpPost]
        public ActionResult Add(Dasaxos d)
        {
            
            context.Dasaxos.Add(d);
            context.SaveChanges();
            return Redirect("/Dasaxos/cucak");
        }
    }
}