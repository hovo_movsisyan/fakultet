﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using asp.das1.Models;

namespace asp.das1.Controllers
{
    public class AmbionController : Controller
    {
        FakultetEntities context;

        public AmbionController()
        {
            this.context = new FakultetEntities();
        }
        // GET: Ambion
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult All()
        {
            ViewBag.ambionner = context.Ambion.ToList();
            return View();
        }
        public ActionResult Dasaxosner(int id)
        {
            var dasaxosner = (from k in context.Dasaxos where k.ambion_id == id select k).ToList();
            ViewBag.dasaxosner = dasaxosner;
            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Ambion x)
        {
            context.Ambion.Add(x);
            context.SaveChanges();

            return Redirect("/Ambion/all"); 
        }
    }
}