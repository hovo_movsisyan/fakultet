﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using asp.das1.Models;

namespace asp.das1.Controllers
{

    public class UsanoxController : Controller
    {
        FakultetEntities context = new FakultetEntities();
        // GET: Usanox
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Cank()
        {
            ViewBag.usanoxner = context.Usanox.ToList();
            return View();
        }
        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.xumb = context.Xumb.ToList();

            return View();
        }
        [HttpPost]
        public ActionResult Add(Usanox d)
        {

            context.Usanox.Add(d);
            context.SaveChanges();
            return Redirect("/Usanox/cank");
        }

        //public ActionResult All()
        //{
        //    ViewBag.usanoxner = context.Usanox.ToList();
        //    return View();
        //}

        //public ActionResult Edit(int id)
        //{
        //    Usanox u = context.Usanox.Where(m => m.id == id).ToList().First();
        //    ViewBag.usanox = u;
        //    return View();
        //}
        [HttpGet]
        public ActionResult All()
        {
            ViewBag.usanoxner = context.Usanox.ToList();
            return View();
        }

        public ActionResult Heracnel(int id)
        {
            //int id = int.Parse(x);
            var u = context.Usanox.Where(m => m.id == id).ToList();
            if(u.Count != 0)
            {
                context.Usanox.Remove(u.First());
                context.SaveChanges();
            }
           
            return Redirect("Usanox/All");
        }
    }
}